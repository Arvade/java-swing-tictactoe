package main.controller;

import main.model.Model;
import main.model.enums.CheckResult;
import main.model.enums.Field;
import main.model.modelutil.BoardChecker;
import main.model.modelutil.BoardPresenter;
import main.model.modelutil.CurrentRoundObserver;
import main.model.modelutil.FieldsObserver;
import main.view.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Controller implements CurrentRoundObserver, FieldsObserver {

    private BoardChecker boardChecker;
    private BoardPresenter boardPresenter;
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;

        this.model.attachObserver((CurrentRoundObserver) this);
        this.model.attachObserver((FieldsObserver) this);
        this.boardPresenter = new BoardPresenter(model, this.view.getFields());
        this.boardChecker = new BoardChecker();

        this.view.addListenerToButtons(new ButtonClickListener());
    }

    @Override
    public void update(String currentRound) {
        this.view.setCurrentRoundIndicator(currentRound);
    }

    @Override
    public void update(Map<Integer, Field> fields) {
        this.view.setFields(fields);
    }

    private void endGame(String winner, List<Integer> fieldIds) {
        this.view.disableBoard();

        int result;

        if (fieldIds.isEmpty()) {
            result = this.view.showWinner(winner);
        } else {
            result = this.view.showWinner(winner, fieldIds);
        }


        this.view.dispose();
        if (result == 0) {
            this.createNewGame();
        }
    }

    private void createNewGame() {
        this.view = new View();
        this.model = new Model();

        this.model.attachObserver((CurrentRoundObserver) this);
        this.model.attachObserver((FieldsObserver) this);
        this.boardPresenter = new BoardPresenter(model, this.view.getFields());
        this.boardChecker = new BoardChecker();

        this.view.addListenerToButtons(new ButtonClickListener());
    }


    class ButtonClickListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            JButton clickedButton = (JButton) actionEvent.getSource();
            clickedButton.setEnabled(false);
            clickedButton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

            Integer buttonIndex = view.getButtonIndex(clickedButton);
            model.setField(buttonIndex);

            Map<CheckResult, Set<Integer>> checkResult = boardChecker.checkBoard(boardPresenter.getAll());

            if (!checkResult.keySet().contains(CheckResult.NOTHING)) {
                String winner = checkResult.keySet().iterator().next().toString();
                List<Integer> fieldIds = new ArrayList<>();
                fieldIds.addAll(checkResult.values().iterator().next());

                endGame(winner, fieldIds);
            }
        }
    }
}
