package main;

import main.controller.Controller;
import main.model.Model;
import main.view.View;

public class Main {
    public static void main(String[] args) {
        View view = new View();
        Model model = new Model();

        Controller controller = new Controller(model, view);
    }
}
