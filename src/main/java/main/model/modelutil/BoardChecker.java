package main.model.modelutil;

import main.model.enums.CheckResult;
import main.model.enums.Field;

import java.util.*;
import java.util.function.Predicate;

public class BoardChecker {


    public Map<CheckResult, Set<Integer>> checkBoard(List<Map<Integer, Field>> input) {

        Predicate<Map.Entry<Integer, Field>> predicateForCircle = integerFieldEntry -> integerFieldEntry.getValue() == Field.CIRCLE;
        Predicate<Map.Entry<Integer, Field>> predicateForCross = integerFieldEntry -> integerFieldEntry.getValue() == Field.CROSS;

        Map<CheckResult, Set<Integer>> output = new HashMap<>();
        for (Map<Integer, Field> row : input) {
            if (row.entrySet().stream().allMatch(predicateForCircle)) {
                output.put(CheckResult.CIRCLE, row.keySet());
                return output;
            } else if (row.entrySet().stream().allMatch(predicateForCross)) {
                output.put(CheckResult.CROSS, row.keySet());
                return output;
            }
        }
        return checkForNoMarkedFields(input);
    }

    private Map<CheckResult, Set<Integer>> checkForNoMarkedFields(List<Map<Integer, Field>> allRows) {
        Predicate<Map.Entry<Integer, Field>> predicateForNoMarkedFields = integerFieldEntry -> integerFieldEntry.getValue() == Field.EMPTY;

        int tempCounter = 0;

        for (Map<Integer, Field> row : allRows) {
            if (row.entrySet().stream().noneMatch(predicateForNoMarkedFields)) {
                tempCounter++;
            }
        }

        Map<CheckResult, Set<Integer>> output = new HashMap<>();
        if (tempCounter >= 8) {
            output.put(CheckResult.FILLED, new HashSet<>());
            return output;
        }

        output.put(CheckResult.NOTHING, new HashSet<>());

        return output;
    }
}
