package main.model.modelutil;

import main.model.enums.Field;

import java.util.Map;

public interface FieldsObserver {
    void update(Map<Integer, Field> fields);
}
