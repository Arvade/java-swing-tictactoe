package main.model.modelutil;

public interface ObservableFields {

    void attachObserver(FieldsObserver observer);

    void detachObserver(FieldsObserver observer);

    void informFieldsObservers();
}
