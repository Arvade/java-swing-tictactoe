package main.model.modelutil;

public interface CurrentRoundObserver {
    void update(String currentRound);
}
