package main.model.modelutil;

import main.model.enums.Field;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoardPresenter implements FieldsObserver {

    private Map<Integer, Field> fields = new HashMap<>();
    private ObservableFields observableFields;

    public BoardPresenter(ObservableFields observableFields, Map<Integer, Field> fields) {
        this.observableFields = observableFields;
        this.fields = fields;

        this.observableFields.attachObserver(this);
    }

    public List<Map<Integer, Field>> getAll() {
        List<Map<Integer, Field>> rowsColumnsDiagonals = new ArrayList<>();


        rowsColumnsDiagonals.add(getFirstRow());
        rowsColumnsDiagonals.add(getSecondRow());
        rowsColumnsDiagonals.add(getThirdRow());

        rowsColumnsDiagonals.add(getFirstColumn());
        rowsColumnsDiagonals.add(getSecondColumn());
        rowsColumnsDiagonals.add(getThirdColumn());

        rowsColumnsDiagonals.add(getFirstDiagonal());
        rowsColumnsDiagonals.add(getSecondDiagonal());

        return rowsColumnsDiagonals;
    }

    private Map<Integer, Field> getFirstRow() {
        Map<Integer, Field> output = new HashMap<>();

        output.put(0, fields.get(0));
        output.put(1, fields.get(1));
        output.put(2, fields.get(2));

        return output;
    }

    private Map<Integer, Field> getSecondRow() {
        Map<Integer, Field> output = new HashMap<>();

        output.put(3, fields.get(3));
        output.put(4, fields.get(4));
        output.put(5, fields.get(5));

        return output;
    }

    private Map<Integer, Field> getThirdRow() {
        Map<Integer, Field> output = new HashMap<>();

        output.put(6, fields.get(6));
        output.put(7, fields.get(7));
        output.put(8, fields.get(8));

        return output;
    }

    private Map<Integer, Field> getFirstColumn() {
        Map<Integer, Field> output = new HashMap<>();

        output.put(0, fields.get(0));
        output.put(3, fields.get(3));
        output.put(6, fields.get(6));

        return output;
    }

    private Map<Integer, Field> getSecondColumn() {
        Map<Integer, Field> output = new HashMap<>();
        output.put(1, fields.get(1));
        output.put(4, fields.get(4));
        output.put(7, fields.get(7));

        return output;
    }

    private Map<Integer, Field> getThirdColumn() {
        Map<Integer, Field> output = new HashMap<>();

        output.put(2, fields.get(2));
        output.put(5, fields.get(5));
        output.put(8, fields.get(8));

        return output;
    }

    private Map<Integer, Field> getFirstDiagonal() {
        Map<Integer, Field> output = new HashMap<>();

        output.put(0, fields.get(0));
        output.put(4, fields.get(4));
        output.put(8, fields.get(8));

        return output;
    }

    private Map<Integer, Field> getSecondDiagonal() {
        Map<Integer, Field> output = new HashMap<>();

        output.put(2, fields.get(2));
        output.put(4, fields.get(4));
        output.put(6, fields.get(6));

        return output;
    }

    @Override
    public void update(Map<Integer, Field> fields) {
        this.fields = fields;
    }
}
