package main.model.modelutil;

import main.model.modelutil.CurrentRoundObserver;

public interface ObservableCurrentRound {

    void attachObserver(CurrentRoundObserver observer);

    void detachObserver(CurrentRoundObserver observer);

    void informCurrentRoundObservers();
}
