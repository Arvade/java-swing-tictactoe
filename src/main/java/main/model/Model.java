package main.model;

import main.model.enums.Field;
import main.model.modelutil.CurrentRoundObserver;
import main.model.modelutil.FieldsObserver;
import main.model.modelutil.ObservableCurrentRound;
import main.model.modelutil.ObservableFields;

import java.util.*;


public class Model implements ObservableFields, ObservableCurrentRound {

    private List<CurrentRoundObserver> currentRoundObservers = new ArrayList<>();
    private List<FieldsObserver> fieldsObservers = new ArrayList<>();

    private Map<Integer, Field> fields = new HashMap<>();
    private Field currentRound;


    public Model() {


        this.setFieldsEmpty();
        this.setCurrentRound(randomWhoStart());

    }

    private Field randomWhoStart() {
        Random random = new Random();

        int randomized = random.nextInt(1);

        if (randomized == 1) return Field.CIRCLE;
        else return Field.CROSS;
    }

    public void setCurrentRound(Field currentRound) {
        this.currentRound = currentRound;
        informCurrentRoundObservers();
    }

    private void changeRound() {
        if (currentRound == Field.CIRCLE) {
            setCurrentRound(Field.CROSS);
        } else if (currentRound == Field.CROSS) {
            setCurrentRound(Field.CIRCLE);
        }
    }

    public Field getCurrentRound() {
        return this.currentRound;
    }

    public void setField(Integer index) {
        this.fields.replace(index, currentRound);
        informFieldsObservers();
        changeRound();
    }

    public void setFieldsEmpty() {
        if (fields.isEmpty()) {
            for (int i = 0; i < 9; i++) {
                fields.put(i, Field.EMPTY);
            }
        } else {
            for (int i = 0; i < 9; i++) {
                fields.replace(i, Field.EMPTY);
            }
        }

        informFieldsObservers();
    }

    @Override
    public void attachObserver(FieldsObserver observer) {
        this.fieldsObservers.add(observer);
    }

    @Override
    public void detachObserver(FieldsObserver observer) {
        this.fieldsObservers.add(observer);
    }

    @Override
    public void informFieldsObservers() {
        for (FieldsObserver observer : fieldsObservers) {
            observer.update(this.fields);
        }
    }

    @Override
    public void attachObserver(CurrentRoundObserver observer) {
        this.currentRoundObservers.add(observer);
    }

    @Override
    public void detachObserver(CurrentRoundObserver observer) {
        this.currentRoundObservers.remove(observer);
    }

    @Override
    public void informCurrentRoundObservers() {
        for (CurrentRoundObserver observer : currentRoundObservers) {
            observer.update(currentRound.toString());
        }
    }
}
