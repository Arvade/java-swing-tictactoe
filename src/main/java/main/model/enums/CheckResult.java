package main.model.enums;

public enum CheckResult {
    CIRCLE("CIRCLE"), CROSS("CROSS"), FILLED("DRAW"), NOTHING("");

    private final String text;

    CheckResult(String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }

}
