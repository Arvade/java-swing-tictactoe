package main.model.enums;

public enum Field {
    CROSS("X"), CIRCLE("O"), EMPTY("");


    private final String text;

    Field(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }
}
