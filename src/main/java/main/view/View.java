package main.view;

import main.model.enums.Field;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class View extends JFrame {

    private List<JButton> boardFields = new ArrayList<>(9);
    private JLabel currentRoundIndicator;


    public View() {
        Container pane = this.getContentPane();
        pane.setLayout(new GridLayout(3, 3));

        this.currentRoundIndicator = new JLabel();


        initializeFields(pane);
        this.setTitle("TicTacToe");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(600, 600);
        this.setResizable(false);
        this.setVisible(true);
        //pane.add(this.currentRoundIndicator);
    }

    public void setFields(Map<Integer, Field> fields) {
        fields.forEach((integer, field) -> {
            boardFields.get(integer).setText(field.toString());
        });
    }

    public int showWinner(String winner) {
        return JOptionPane.showConfirmDialog(this, "THE W-W-WINNER IS " + winner + "!\n\n Would you like to play again?");
    }

    public int showWinner(String winner, List<Integer> fieldIds) {
        System.out.println("fieldIds.size() = " + fieldIds.size());
        for (Integer index : fieldIds) {
            this.boardFields.get(index).setBackground(Color.GREEN);
        }
        return JOptionPane.showConfirmDialog(this, "THE W-W-WINNER IS " + winner + "!\n\n Would you like to play again?");
    }

    public void disableBoard() {
        for (JButton button : boardFields) {
            button.setEnabled(false);
            button.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public void initializeFields(Container pane) {
        for (int i = 0; i < 9; i++) {
            JButton button = new JButton();
            button.setSize(100, 100);
            Font currentFont = new Font("Arial", Font.PLAIN, 100);
            button.setFont(currentFont);
            button.setCursor(new Cursor(Cursor.HAND_CURSOR));

            boardFields.add(button);
            pane.add(button);
        }
    }

    public void setField(JButton clickedField, String value) {
        for (JButton field : boardFields) {
            if (field == clickedField) {
                field.setText(value);
                field.setEnabled(false);
                field.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        }
    }

    public Map<Integer, Field> getFields() {
        Map<Integer, Field> output = new HashMap<>();

        for (int i = 0; i < boardFields.size(); i++) {
            JButton button = boardFields.get(i);
            String buttonText = button.getText();

            if (buttonText.equalsIgnoreCase(Field.CIRCLE.toString())) {
                output.put(i, Field.CIRCLE);
            } else if (buttonText.equalsIgnoreCase(Field.CROSS.toString())) {
                output.put(i, Field.CROSS);
            } else {
                output.put(i, Field.EMPTY);
            }
        }

        return output;
    }

    public void setCurrentRoundIndicator(String value) {
        this.currentRoundIndicator.setText("Current round: " + value);
    }

    public void addListenerToButtons(ActionListener actionListener) {
        for (JButton field : boardFields) {
            field.addActionListener(actionListener);
        }
    }

    public Integer getButtonIndex(JButton button) {
        return boardFields.indexOf(button);
    }
}
